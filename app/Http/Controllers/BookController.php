<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Illuminate\Support\Str;

class BookController extends Controller
{
    //

    public function addBook(Request $request){
        $validator = Validator::make($request->all(), [
            'book_name' => 'required|string|max:255|unique:books',
            'author' => 'required|string|max:255',
            'cover_image' => 'required|string'
        ]);

        if($validator->fails()){
                return response()->json($validator->errors(), 403);
        }

        $data = base64_decode($request->cover_image);
        $file_name = Str::random(36).'.png';
        $destinationPath = public_path();
        file_put_contents($destinationPath.'/'.$file_name, $data);

        $book = new Book();
        $book->book_name = $request->book_name;
        $book->author = $request->author;
        $book->cover_image = request()->getHttpHost().'/'.$file_name;
        $book->user_id = JWTAuth::user()->u_id;
        $book->save();

        return response()->json([
            'message'=>'Successfully added book'
        ],200);

    }

    public function updateBook(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:books,b_id',
            'book_name' => 'required|string|max:255',
            'author' => 'required|string|max:255',
            'cover_image' => 'required|string'
        ]);

        if($validator->fails()){
                return response()->json($validator->errors(), 403);
        }

        $check_duplicate = Book::where('b_id', '!=', $request->id)
                            ->where('book_name',$request->book_name)->get();
        if(count($check_duplicate) >= 1){
                return response()->json(['book_name'=>['The book name has already been taken']], 403);
        }

        $data = base64_decode($request->cover_image);
        $file_name = Str::random(36).'.png';
        $destinationPath = public_path();
        file_put_contents($destinationPath.'/'.$file_name, $data);

        $book = Book::find($request->id);;
        $book->book_name = $request->book_name;
        $book->author = $request->author;
        $book->cover_image = request()->getHttpHost().'/'.$file_name;
        $book->user_id = JWTAuth::user()->u_id;
        $book->save();

        return response()->json([
            'message'=>'Successfully updated book'
        ],200);

    }

    public function deleteBook(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:books,b_id'
        ]);

        if($validator->fails()){
                return response()->json($validator->errors(), 403);
        }

        $book = Book::find($request->id);
        $book->delete();

        return response()->json([
            'message'=>'Successfully deleted book'
        ],200);

    }

    public function listBook(){
        $books = Book::where('user_id',JWTAuth::user()->u_id)->get();
        return response()->json(compact('books'),200);
    }
}
