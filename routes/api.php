<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user/profile', 'UserController@getAuthenticatedUser');

    Route::post('book/add', 'BookController@addBook');
    Route::post('book/update', 'BookController@updateBook');
    Route::post('book/delete', 'BookController@deleteBook');
    Route::get('book/list', 'BookController@listBook');
});
